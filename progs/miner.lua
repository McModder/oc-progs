local term = require("term")
local fs = require("filesystem")

if not require("component").isAvailable("robot") then error("This program need a robot") end

local args = {...}
-- Config
local owner = args[1] or "Nik_mmzd (McModder)"
-- End

local isError = false
if not fs.exists("/lib/menuAPI.lua") then print("Missing menuAPI");isError=true end
if not fs.exists("/lib/digAPI.lua") then print("Missing digAPI");isError=true end
if isError then print("Missing APIs, download it via updater: pastebin.com/BW1fux3Z");os.exit() else isError=nil end

local menuAPI = require("menuAPI")
local digAPI = require("digAPI")

-- begin
local menu = {"tonnel", "endless tonnel", "room", "endless room", "passages", "cobblegen"} -- for easy adding new modes
local chesttypes = {"chest", "enderchest"} -- see above
term.setCursorBlink(false)
local ret = menuAPI.menu(menu, "Select mode:")
local enderchest = menuAPI.menu(chesttypes, "Select chest type:")
if enderchest == "chest" then enderchest = false elseif enderchest == "enderchest" then enderchest = true end
digAPI.fuel()
if ret == "endless tonnel" then
  digAPI.info(owner, enderchest)
  digAPI.tonnel(owner, enderchest, length)
elseif ret == "tonnel" then
  term.clear()
  print("Set tonnel length:")
  local length = io.read("*number")
  digAPI.tonnel(owner, enderchest, length)
elseif ret == "endless room" then
  term.clear()
  print("Set start side length")
  local length = io.read("*number")
  digAPI.room(owner, length, enderchest)
elseif ret == "room" then
  term.clear()
  print("Set start side length")
  local length = io.read("*number")
  print("Set side length limit")
  local limit = tonumber(io.read("*number"))
  digAPI.room(owner, length, enderchest, limit)
elseif ret == "passages" then
  term.clear()
  print("Set one passage length")
  local length = io.read("*number")
  digAPI.passages(owner, length, enderchest)
elseif ret == "cobblegen" then
  digAPI.cobblegen(owner)
else
  term.setCursorBlink(true)
  term.clear()
  print("In development")
  os.exit()
end