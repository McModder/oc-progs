local comp = require("component")
local robot = comp.robot
local computer = require("computer")
local fs = require("filesystem")
local seri = require("serialization")

-- CONFIG
local config
if not fs.isDirectory("/etc/harvester/") then fs.makeDirectory("/etc/harvester/") end

if fs.exists("/etc/harvester/config.cfg") then
  local file = io.open("/etc/harvester/config.cfg", "r")
  local cfgText = file:read("*a")
  config = seri.unserialize(cfgText)
  file:close()
else
  config = {width=0, length=0, heigth=0, useRightClick=false, startFromLeftSide=false, sleep=60}
  local file = io.open("/etc/harvester/config.cfg", "w")
  if not file then error("Can't open config file for write") end
  file:write(seri.serialize(config, true))
  file:close()
  print("Plese configure robot using /etc/harvester/config.cfg")
  os.exit()
end
fs, seri = nil, nil

local function moveRobot( direction )
  local moved
  repeat
    moved = robot.move(direction)
  until moved == true
  return true
end

local breakMethod = config.useRightClick==true and robot.use or robot.swing

comp.computer.beep(1000, 0.5)
robot.setLightColor(0x00CC00)
while true do
  local clockwise = config.startFromLeftSide
  for i=1,config.heigth do
    moveRobot(1)
  end
  moveRobot(3)
  robot.setLightColor(0x0066CC)
  for width=1,config.width-1 do
    for length=1,config.length-1 do
      breakMethod(0)
      comp.tractor_beam.suck()
      moveRobot(3)
    end
    breakMethod(0)
    comp.tractor_beam.suck()
    robot.turn(clockwise)
    moveRobot(3)
    robot.turn(clockwise)
    clockwise = not clockwise
  end
  for length=1,config.length-1 do
    breakMethod(0)
    comp.tractor_beam.suck()
    moveRobot(3)
  end
  breakMethod(0)
  -- all harvested. we need to go to base.
  robot.setLightColor(0x6600CC)
  if clockwise == config.startFromLeftSide then
    -- we need to go back
    robot.turn(true)
    robot.turn(true)
    for length=1,config.length-1 do moveRobot(3) end
  end
  robot.turn(config.startFromLeftSide)
  for width=1,config.width-1 do moveRobot(3) end
  -- go to base
  robot.turn(not config.startFromLeftSide)
  moveRobot(3)
  for i=1,config.heigth do
    moveRobot(0)
  end
  --recharge and unload here
  robot.setLightColor(0xFF8000)
  for i=1,robot.inventorySize() do
    if robot.count(i)>0 then 
      robot.select(i)
      robot.drop(3)
    end
  end
  robot.select(1)
  robot.turn(true)
  robot.turn(true)
  robot.setLightColor(0xFFFF33)
  local sleep=config.sleep
  while computer.energy()/computer.maxEnergy()<0.95 do
    os.sleep(10)
    sleep=sleep-10
  end
  robot.setLightColor(0x66FFFF)
  if sleep>0 then os.sleep(sleep) end
end