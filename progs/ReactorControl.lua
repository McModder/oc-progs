local buffer = require("doubleBuffering")
local GUI = require("GUI")
local component = require("component")
local unicode = require("unicode")
local event = require("event")
local files =require("files")
local sides = require("sides")
local fs = require("filesystem")

--[[
TODO
Детект перегрева

]]

if not component.isAvailable("reactor_chamber") and not component.isAvailable("reactor_redstone_port") and not component.isAvailable("br_reactor") then
  GUI.error("Не найдены реакторы. Нажмите \"ОК\" и подключите реактор(ы)", {title={color=0xFF8888, text="Информация"}})
end

if component.isAvailable("reactor_redstone_port") then
GUI.error("Найден жидкостный реактор.\nВажная информация: в OpenComputers неполная поддержка жидкостных реакторов. Но автовыключение по температуре/заполненности энергохранилища при соотв. настройке работать будет", {title={color=0xFF8888, text="Информация"}})
end

buffer.start()

saved = {}

local function load()
  if fs.exists("MineOS/System/ReactorControl/settings.cfg") then
    saved = files.loadTableFromFile("MineOS/System/ReactorControl/settings.cfg")
  end
end

local function save()
  files.saveTableToFile("MineOS/System/ReactorControl/settings.cfg", saved)
end

local function getProxies(name)
  --print("Start", name)
  if name == nil then return end
  for addr in pairs(component.list(name)) do
    table.insert(proxies, component.proxy(addr))
    --print(addr, name)
  end
end

local function getReactorType(Rtype)
  if Rtype == "reactor_chamber" then return "Реактор"
  elseif Rtype == "reactor_redstone_port" then return "Жидкостный реактор"
  elseif Rtype == "br_reactor" then return "BigReactor"
  end
end

local function getReactors()
  reactors = {}
  proxies = {}
  getProxies("reactor_chamber")
  getProxies("reactor_redstone_port")
  getProxies("br_reactor")

  for i=1,#proxies do
    local reactor = {}
    reactor.type = getReactorType(proxies[i].type)
    reactor.proxy = proxies[i]
    table.insert(reactors, reactor)
  end
end

local function getReactorsIsActive(i)
  if proxies[i].type == "br_reactor" then
    if proxies[i].getActive() == true then return "ВКЛЮЧЕН" else return "ВЫКЛЮЧЕН" end
  else
    if proxies[i].producesEnergy() == true then return "ВКЛЮЧЕН" else return "ВЫКЛЮЧЕН" end
  end
end

function tickToTime(ticks, enabled)
  if enabled == false then return "error" end
  local seconds = ticks/20
  local min = seconds % 3600
  return math.floor(seconds/3600)..":"..math.floor(min/60)..":"..math.floor(min%60)
end

function getBRenergy(reactorProxy)
  if saved[reactorProxy.address].energyAddr then
    if component.get(saved[reactorProxy.address].energyAddr) then
      return component.proxy(saved[reactorProxy.address].energyAddr).getEnergyStored().." ("..math.floor(component.proxy(saved[reactorProxy.address].energyAddr).getEnergyStored()/component.proxy(saved[reactorProxy.address].energyAddr).getMaxEnergyStored()*100).."%)"
    end
  end
  return reactorProxy.getEnergyStored().." ("..math.floor(reactorProxy.getEnergyStored()/100000).."%)"
end

load()
getProxies()
getReactors()

local buttons = {}

local function drawAll()
  buffer.clear()
  for i=1,#reactors do
    local tempProxy = reactors[i].proxy
    saved[tempProxy.address] = saved[tempProxy.address] or {}
    saved[tempProxy.address].energyTrack = saved[tempProxy.address].energyTrack or false
    if tempProxy.type == "br_reactor" then
      --BigReactors
      buttons[i] = GUI.buttons(1, (3*i+1), GUI.directions.horizontal, 1,
        {GUI.buttonTypes.framedDefault, 12, 3, 0xCCCCCC, 0xff0000, 0xFF8888, 0xFFFFFF, getReactorsIsActive(i)},
        {GUI.buttonTypes.framedDefault, 14, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF, "Управление"},
        {GUI.buttonTypes.framedDefault, buffer.screen.width-28, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF,
          reactors[i].type..": "..math.floor(tempProxy.getEnergyProducedLastTick()).." RF/t, осталось: "..tickToTime((tempProxy.getFuelAmount()/tempProxy.getFuelConsumedLastTick())/20, tempProxy.getActive())..", слежение: "..tostring(saved[tempProxy.address].energyTrack)..", энергии: "..getBRenergy(reactors[i].proxy)
        })
    else
      --IC2
      buttons[i] = GUI.buttons(1, (3*i+1), GUI.directions.horizontal, 1,
        {GUI.buttonTypes.framedDefault, 12, 3, 0xCCCCCC, 0xff0000, 0xFF8888, 0xFFFFFF, getReactorsIsActive(i)},
        {GUI.buttonTypes.framedDefault, 14, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF, "Настройка"},
        {GUI.buttonTypes.framedDefault, buffer.screen.width-28, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF,
          reactors[i].type..": "..tempProxy.getReactorEUOutput().." EU/t, нагрев: "..tempProxy.getHeat().."("..math.floor(tempProxy.getHeat()/tempProxy.getMaxHeat()*100).."%)"..", слежение: "..tostring(saved[tempProxy.address].energyTrack)
        })
    end
  end

  manageButt = GUI.buttons(buffer.screen.width/2-11, (1), GUI.directions.horizontal, 2,
      {GUI.buttonTypes.framedDefault, 10, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF, "Обновить"},
      {GUI.buttonTypes.framedDefault, 10, 3, 0xCCCCCC, 0xc0edff, 0xFF8888, 0xFFFFFF, "Выйти"}
      )
  buffer.draw()
end

local function reactorTracking(TTreactor)
  saved[TTreactor.address] = saved[TTreactor.address] or {}
  --BR
  if TTreactor.type == "br_reactor" and saved[TTreactor.address].energyTrack == true then
    if component.get(saved[TTreactor.address].energyAddr) then
      if math.floor(component.proxy(saved[TTreactor.address].energyAddr).getEnergyStored()/component.proxy(saved[TTreactor.address].energyAddr).getMaxEnergyStored()*100) > 95 then
        TTreactor.setActive(false)
      elseif math.floor(component.proxy(saved[TTreactor.address].energyAddr).getEnergyStored()/component.proxy(saved[TTreactor.address].energyAddr).getMaxEnergyStored()*100) < 10 then
        TTreactor.setActive(true)
      end
    else
      if math.floor(TTreactor.getEnergyStored()/100000) > 95 then
        TTreactor.setActive(false)
      elseif math.floor(TTreactor.getEnergyStored()/100000) < 10 then
        TTreactor.setActive(true)
      end
    end
  elseif TTreactor.type == "br_reactor" then return
  else
    --IC2
    if component.get(tostring(saved[TTreactor.address].redstone)) == nil and math.floor(TTreactor.getHeat()/TTreactor.getMaxHeat()*100) > 90 then
      GUI.error("Перегрев реактора!\nНевозможно выключить автоматически!", {title={color=0xFF8888, text="Перегрев реактора!"}})
    elseif component.get(tostring(saved[TTreactor.address].redstone)) == nil then
      return
    elseif math.floor(TTreactor.getHeat()/TTreactor.getMaxHeat()*100) > 90 then
      component.proxy(saved[TTreactor.address].redstone).setOutput(sides[saved[TTreactor.address].rsSide], 0)
    elseif math.floor(TTreactor.getHeat()/TTreactor.getMaxHeat()*100) < 10 and saved[TTreactor.address].energyTrack == true and TTreactor.producesEnergy() == false and component.get(saved[TTreactor.address].redstone) ~= nil and math.floor(component.proxy(saved[TTreactor.address].energyAddr).getStored()/component.proxy(saved[TTreactor.address].energyAddr).getCapacity()*100) < 10 then
      component.proxy(saved[TTreactor.address].redstone).setOutput(sides[saved[TTreactor.address].rsSide], 15)
    elseif component.get(saved[TTreactor.address].redstone) ~= nil and saved[TTreactor.address].energyTrack == true then
      if math.floor(component.proxy(saved[TTreactor.address].energyAddr).getStored()/component.proxy(saved[TTreactor.address].energyAddr).getCapacity()*100) > 95 then
        component.proxy(saved[TTreactor.address].redstone).setOutput(sides[saved[TTreactor.address].rsSide], 0)
      elseif math.floor(component.proxy(saved[TTreactor.address].energyAddr).getStored()/component.proxy(saved[TTreactor.address].energyAddr).getCapacity()*100) < 10 and math.floor(TTreactor.getHeat()/TTreactor.getMaxHeat()*100) < 10 then
        component.proxy(saved[TTreactor.address].redstone).setOutput(sides[saved[TTreactor.address].rsSide], 15)
      end
    end
  end
end

local function massivLen(massiv)
  local answer = 0
  for a in pairs(massiv) do
    answer = answer + 1
  end
  return answer
end

local function compList(massiv, comp, text)
  if massivLen(component.list(comp)) ~= 0 then
    if text then table.insert(massiv, text) end
    for addr, name in pairs(component.list(comp)) do
      table.insert(massiv, addr)
    end
  end
end

local function detectAllRedstoneIO()
  local toReturn = {}
  compList(toReturn,"redstone")
  return toReturn
end

local function detectAllEUCapacitors()
  local toReturn = {}
  if massivLen(component.list("cesu"))+massivLen(component.list("mfe"))+massivLen(component.list("mfsu"))~=0 then
    table.insert(toReturn, "-- энергохранилища --")
    compList(toReturn, "cesu", "--- CESUs ---")
    compList(toReturn, "mfe", "--- MFEs ---")
    compList(toReturn, "mfsu", "--- MFSUs ---")
  end
  if massivLen(component.list("chargepad_cesu"))+massivLen(component.list("chargepad_mfe"))+massivLen(component.list("chargepad_mfsu"))~=0 then
    table.insert(toReturn, "-- Заряжающие плиты --")
    compList(toReturn, "chargepad_cesu", "--- CESUs ---")
    compList(toReturn, "chargepad_mfe", "--- MFEs ---")
    compList(toReturn, "chargepad_mfsu", "--- MFSUs ---")
  end
  return toReturn
end

local function detectAllTECapacitors()
  local toReturn = {}
  if massivLen(component.list("thermalexpansion_cell"))~=0 then
    compList(toReturn, "thermalexpansion_cell", "--- Energy Cells ---")
  end
  return toReturn
end

drawAll()

while true do
  local e = {event.pull(5)}
  pressed = {}
  if e[1] == "component_added" or e[1] == "component_removed" then
    getProxies()
    getReactors()
  end
  if e[1] == "touch" then
    for _,butt in pairs(manageButt) do
      if butt:isClicked(e[3], e[4]) then
        butt:press(0.3)
        if butt.text == "Обновить" then
          for i=1,#reactors do
            saved[reactors[i].proxy.address] = saved[reactors[i].proxy.address] or {}
            reactorTracking(reactors[i].proxy)
          end
          buffer.clear()
          drawAll()
        elseif butt.text == "Выйти" then
          return
        end
      end
    end
    for num,reac in pairs(buttons) do
      for _,butt in pairs(reac) do
        if butt:isClicked(e[3], e[4]) then
          butt:press(0.3)
          if butt.text == "ВКЛЮЧЕН" then
            if reactors[num].proxy.type == "br_reactor" then
              reactors[num].proxy.setActive(false)
              saved[reactors[num].proxy.address].energyTrack = false
            else
              saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
              if saved[reactors[num].proxy.address].redstone == nil then
                GUI.error("Не настроен Redstone I/O для этого реактора, воспользуйтесь кнопкой \"Настройка\"", {title={color=0xFF8888, text="Ошибка"}})
              else
                component.proxy(saved[reactors[num].proxy.address].redstone).setOutput(sides[saved[reactors[num].proxy.address].rsSide], 0)
                saved[reactors[num].proxy.address].energyTrack = false
              end
            end
          elseif butt.text == "ВЫКЛЮЧЕН" then
            if reactors[num].proxy.type == "br_reactor" then
              reactors[num].proxy.setActive(true)
            else
              saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
              if saved[reactors[num].proxy.address].redstone == nil then
                GUI.error("Не настроен Redstone I/O для этого реактора, воспользуйтесь кнопкой \"Настройка\"", {title={color=0xFF8888, text="Ошибка"}})
              else
                component.proxy(saved[reactors[num].proxy.address].redstone).setOutput(sides[saved[reactors[num].proxy.address].rsSide], 15)
              end
            end
          elseif butt.text == "Настройка" then
            saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
            local data = ecs.universalWindow("auto", "auto", 56, 0x000000, true,
              {"EmptyLine"},
              {"CenterText", ecs.colors.orange, "Выберите и настройте Redstone I/O"},
              {"EmptyLine"},
              {"Selector", 0xffffff, ecs.colors.orange, saved[reactors[num].proxy.address].redstone or "Выберите Redstone I/O", table.unpack(detectAllRedstoneIO())},
              {"Selector", 0xffffff, ecs.colors.orange, saved[reactors[num].proxy.address].rsSide or "Выберите сторону", "bottom", "top", "back", "front", "right", "left"},
              {"EmptyLine"},
              {"CenterText", ecs.colors.orange, "Выберите энергохранилище реактора"},
              {"EmptyLine"},
              {"Selector", 0xffffff, ecs.colors.orange, saved[reactors[num].proxy.address].energyAddr or "Выберите энергохранилище", table.unpack(detectAllEUCapacitors())},
              {"Switch", 0xF2B233, 0xffffff, 0xc0edff, "Следить за зарядом", saved[reactors[num].proxy.address].energyTrack or false},
              {"EmptyLine"},
              {"Button", {ecs.colors.orange, 0xffffff, "OK"}, {0x999999, 0xffffff, "ТТ"}, {0x777777, 0xffffff, "Отмена"}}
            )
            if data[5] == "OK" or data[5] == "ТТ" then
              local Taddress = component.get(data[1])
              if Taddress == nil then GUI.error("Неверный адрес компонента", {title={color=0xFF8888, text="Ошибка"}}) else
                saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
                saved[reactors[num].proxy.address].redstone = Taddress
                saved[reactors[num].proxy.address].rsSide = data[2]
                save()
              end
              local Taddress = nil
              local Taddress = component.get(data[3])
              if Taddress == nil then GUI.error("Неверный адрес компонента", {title={color=0xFF8888, text="Ошибка"}}) else
                saved[reactors[num].proxy.address].energyAddr = Taddress
                saved[reactors[num].proxy.address].energyTrack = data[4]
                save()
              end
            end
          elseif butt.text == "Управление" then
            saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
            local data = ecs.universalWindow("auto", "auto", 36, 0x000000, true,
              {"EmptyLine"},
              {"Selector", 0xffffff, ecs.colors.orange, saved[reactors[num].proxy.address].energyAddr or "Выберите энергохранилище", "Буфер реактора", table.unpack(detectAllTECapacitors())},
              {"Switch", 0xF2B233, 0xffffff, 0xc0edff, "Следить за зарядом", saved[reactors[num].proxy.address].energyTrack or false},
              {"Slider", ecs.colors.orange, ecs.colors.black, 0, 100, reactors[num].proxy.getControlRodLevel(1), "Опустить контрольные стержни на ", "%"},
              {"EmptyLine"},

              {"Button", {ecs.colors.orange, 0xffffff, "OK"}, {0x999999, 0xffffff, "ТТ"}, {0x777777, 0xffffff, "Отмена"}}
            )
            if data[4] == "OK" or data[4] == "ТТ" then
              local Taddress = component.get(data[1])
              if Taddress == nil and data[1] ~= "Буфер реактора" then GUI.error("Неверный адрес компонента", {title={color=0xFF8888, text="Ошибка"}}) else
                saved[reactors[num].proxy.address] = saved[reactors[num].proxy.address] or {}
                saved[reactors[num].proxy.address].energyAddr = Taddress or "Буфер реактора"
                saved[reactors[num].proxy.address].energyTrack = data[2]
                reactors[num].proxy.setAllControlRodLevels(data[3])
                save()
              end
            end
          end
        end
      end
    end
    for i=1,#reactors do
      saved[reactors[i].proxy.address] = saved[reactors[i].proxy.address] or {}
      reactorTracking(reactors[i].proxy)
    end
    drawAll()
  else
    for i=1,#reactors do
      saved[reactors[i].proxy.address] = saved[reactors[i].proxy.address] or {}
      reactorTracking(reactors[i].proxy)
    end
    buffer.clear()
    drawAll()
  end
end