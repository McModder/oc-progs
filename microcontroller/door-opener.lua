local rightName = "entryBunker"
local side = 5
local time = 3
----
local rs = component.proxy(component.list("redstone")())
local comp = component.proxy(component.list("computer")())
local reader = component.proxy(component.list("os_rfidreader")())

local function find( Ttable, text )
  for _, value in pairs(Ttable) do
    if value == text then return true end
  end
  return false
end

local function wait(num)
  local wait = ""
  repeat
    wait = computer.pullSignal(num or 1)
  until wait == nil
end

local cards = reader.scan(5)
local hasRights = false
for _,card in pairs(cards) do
  local rights = {}
  for right in string.gmatch(card.data, "[^%;]+") do
    table.insert(rights, right)
  end
  if card.data == "allAccess" or find(rights, rightName) == true then
    hasRights = true
    break
  end
end

if hasRights == true then
  local color = rs.getBundledInput(2, 15) > 0 and 14 or 13
  rs.setOutput(side, 2)
  wait(time)
  rs.setOutput(side, 0)
else
  comp.beep(1000, 0.5)
  comp.beep(1000, 0.5)
end
computer.shutdown()