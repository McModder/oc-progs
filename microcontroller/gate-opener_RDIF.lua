local rightName = "railwayGate"
local side = 2
local emit = true
local repeats = 3
----
local rs = component.proxy(component.list("redstone")())
local comp = component.proxy(component.list("computer")())
local reader = component.proxy(component.list("os_rfidreader")())

local function find( Ttable, text )
  for _, value in pairs(Ttable) do
    if value == text then return true end
  end
  return false
end

local function wait(num)
  local wait = ""
  repeat
    wait = computer.pullSignal(num or 1)
  until wait == nil
end

local cards = reader.scan(5)
local hasRights = false
for _,card in pairs(cards) do
  local rights = {}
  for right in string.gmatch(card.data, "[^%;]+") do
    table.insert(rights, right)
  end
  if card.data == "allAccess" or find(rights, rightName) == true then
    hasRights = true
    break
  end
end

if hasRights == true then
  if emit == true then rs.setBundledOutput(side, {[12]=100}) end
  local color = rs.getBundledInput(2, 15) > 0 and 14 or 13
  for i=1,repeats do
    rs.setBundledOutput(side, {[color]=100})
    wait(1)
    rs.setBundledOutput(side, {[color]=0})
    wait(1)
  end
  if emit == true then rs.setBundledOutput(side, {[12]=0}) end
else
  comp.beep(1000, 0.5)
  comp.beep(1000, 0.5)
end
computer.shutdown()