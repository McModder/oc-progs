-- MenuAPI 2
local term = require("term")
local event = require("event")

local menuAPI = {}

function printElement(text, pos, selected)
  term.setCursor(1, pos)
  term.clearLine()
  print((selected and "> " or "  ")..text)
  return pos
end

function menuAPI.menu(elements, header)
  if header == nil then header = "Select:" end
  term.clear()
  term.setCursor(1, 1)
  print(header)
  local selected = 1
  for pos,element in ipairs(elements) do
    term.setCursor(1, 1+pos)
    printElement(element, pos+1, pos == selected and true or false)
  end
  while true do
    local pulledEvent, _, _, data = event.pull()
    if pulledEvent == "key_down" then
      if data == 28 then
        return elements[selected]
      elseif (data == 200 and selected ~= 1) or (data == 208 and selected ~= #elements) then
        printElement(elements[selected], selected+1, false)
        selected = data == 200 and selected-1 or selected+1
        printElement(elements[selected], selected+1, true)
      elseif (data == 200 and selected == 1) or (data == 208 and selected == #elements) then
        printElement(elements[selected], selected+1, false)
        selected = data == 200 and #elements or 1
        printElement(elements[selected], selected+1, true)
      end
    elseif pulledEvent == "touch" then
      for y,element in ipairs(elements) do
        if y+1 == data then return element end
      end
    end
  end
end

return menuAPI