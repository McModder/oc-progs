-- digAPI
local term = require("term")
local robot = require("robot")
local comp = require("component")
local gener = comp.generator
local computer = require("computer")
local fs = require("filesystem")
local seri = require("serialization")
local telegram = comp.isAvailable("internet") and require("telegram") or nil

-- CONFIG
local configTable
if fs.isDirectory("/etc/miner/") then
  if fs.exists("/etc/miner/config.cfg") then
    local file = io.open("/etc/miner/config.cfg", "r")
    local cfgText = file:read("*a")
    configTable = seri.unserialize(cfgText)
    file:close()
  else
    configTable = {broadcastErrors = false, internetErrors = true, dialogID = ""}
    local file = io.open("/etc/miner/config.cfg", "w")
    if not file then error("Can't open config file for write") end
    file:write(seri.serialize(configTable, true))
    file:close()
  end
else
  fs.makeDirectory("/etc/miner/")
  configTable = {broadcastErrors = false, internetErrors = true, dialogID = ""}
  local file = io.open("/etc/miner/config.cfg", "w")
  if not file then error("Can't open config file for write") end
  file:write(seri.serialize(configTable, true))
  file:close()
end

local broadcastErrors = configTable.broadcastErrors or false
local internetErrors = configTable.internetErrors or true
local dialogID = configTable.dialogID or ""
if errorsAddr == "" or dialogID == "" or internet == nil then internetErrors = false end -- DON'T TOUCH!

seri, fs, configTable = nil, nil, nil


local digAPI = {}
_G.minerData = {}

function digAPI.send( toSend )
  if telegram and internetErrors then
    telegram.send(dialogID, string.format("Robot %s reports.\n%s", robot.name(), toSend))
  end
end

function digAPI.error( errorText )
  errorText = errorText or "ERROR. PROGRAM WILL BE TERMINATE"
  errorText = errorText..". Mode "..tostring(minerData.mode)..". Length "..(minerData.length or minerData.room or "nil")
  if comp.isAvailable("modem") then
    if comp.modem.isWireless() and broadcastErrors then
      print("Wireless Modem is available, broadcasting error")
      comp.modem.broadcast(23, errorText)
    end
  end
  if internet and internetErrors then
    print("Internet Card is available, broadcasting error")
    digAPI.send(errorText)
  end
  computer.shutdown()
end

function digAPI.fuel(count)
  count = count or 8
  if gener.count() < count then
    robot.select(1)
    if robot.count(1) == 0 then digAPI.error("No fuel") end
    if not gener.insert(count-gener.count()) then digAPI.error("No fuel") end
  end
end

function digAPI.recharge()
  if computer.energy() <= computer.maxEnergy()/10 then
    repeat
      digAPI.fuel()
      print("!! ERROR !!\nRobot is recharging\nTotal charge: "..tostring(computer.energy()).."\nNeeded charge: "..computer.maxEnergy()*0.9)
      if internet and internetErrors then digAPI.send("Started robot recharging. Energy "..tostring(math.floor(computer.energy()))..". Fuel: "..gener.count()+robot.count(1)) end
      os.sleep(30)
    until computer.energy() >= computer.maxEnergy()*0.9
  end
end

function digAPI.pole(owner ,repeats, enderchest, unloadSide, show_recursive_level)
  local show_recursive_level = show_recursive_level or true
  local unloadSide = unloadSide or "left"
  for i=1,repeats do
    repeat
      robot.swing()
    until robot.forward()
    robot.swingUp()
    robot.swingDown()
    digAPI.fuel()
    if robot.count(robot.inventorySize()) ~= 0 then digAPI.unload(enderchest, unloadSide) end
    digAPI.info(owner, enderchest)
    if show_recursive_level then print("Room side: " .. repeats .. " blocks") end
    digAPI.recharge()
  end
end

function digAPI.passages(owner, length, enderchest)
  minerData.mode = "passages"
  digAPI.send("Robot started. Mode passages. Passages length "..tostring(length)..". Enderchest "..tostring(enderchest))
  while true do
    digAPI.pole(owner, length, enderchest, "back", false)
    robot.turnAround()
    digAPI.pole(owner, length*2, enderchest, "back", false)
    robot.turnAround()
    digAPI.pole(owner, length, enderchest, "back", false)
    robot.turnLeft()
    digAPI.pole(owner, 3, enderchest, "back", false)
    robot.turnRight()
  end
end

function digAPI.room(owner, length, enderchest, limit)
  minerData.mode = "room"
  length = length or 1
  if enderchest == nil then enderchest = false end
  digAPI.send("Robot started. Mode room. Limit "..(limit == nil and "unlimited" or tostring(limit))..". Enderchest "..tostring(enderchest))
  if limit == nil then
    while true do
      minerData.room = length
      digAPI.pole(owner, length, enderchest)
      robot.turnLeft()
      digAPI.pole(owner, length, enderchest)
      robot.turnLeft()
      length = length + 1
    end
  else
    while tonumber(length) < tonumber(limit) do
      minerData.room = length
      digAPI.pole(owner, length, enderchest)
      robot.turnLeft()
      digAPI.pole(owner, length, enderchest)
      robot.turnLeft()
      length = length + 1
    end
    digAPI.send("Robot ended. Mode room. Limit "..(limit == nil and "unlimited" or tostring(limit))..". Enderchest "..tostring(enderchest))
  end
end

function digAPI.slice(owner, enderchest)
  digAPI.pole(owner, 1, enderchest, "back", false)
  robot.turnLeft()
  digAPI.pole(owner, 1, enderchest, "back", false)
  robot.turnAround()
  digAPI.pole(owner, 2, enderchest, "back", false)
  robot.turnAround()
  digAPI.pole(owner, 1, enderchest, "back", false)
  robot.turnRight()
end

function digAPI.tonnel( owner, enderchest, length )
  minerData.mode = "tonnel"
  enderchest = enderchest or false
  length = length or math.huge
  digAPI.send("Robot started. Mode tonnel. Limit "..(length == math.huge and "unlimited" or tostring(length))..". Enderchest "..tostring(enderchest))
  for i=1,length do
    minerData.length = i
    digAPI.slice(owner, enderchest)
    digAPI.fuel()
    digAPI.info(owner, enderchest)
    digAPI.recharge()
    if robot.count(robot.inventorySize()) ~= 0 then digAPI.unload(enderchest) end
  end
  digAPI.send("Robot ended. Mode tonnel. Limit "..(length == math.huge and "unlimited" or tostring(length))..". Enderchest "..tostring(enderchest))
end

function digAPI.unload(enderchest, side)
  side = side or "none"
  if side == "left" then robot.turnLeft() else robot.turnAround() end
  robot.select(2)
  if robot.count(2) == 0 then digAPI.error("No chests") end
  local tempMethods = {drop = robot.drop, swing = robot.swing}
  if not robot.place() then if not robot.placeDown() then digAPI.error("Cant place a chest to unload") else tempMethods = {drop = robot.dropDown, swing = robot.swingDown} end end
  
  for i = 3, robot.inventorySize() do
    robot.select(i)
    tempMethods.drop()
  end
  digAPI.send("Robot unloaded.")
  if enderchest then
    robot.select(2)
    tempMethods.swing()
  end
  robot.select(3)
  if side == "left" then robot.turnRight() else robot.turnAround() end
end

function digAPI.info(name, enderchest)
  term.clear()
  term.setCursor(1, 1)
  print("robot owner: " .. name)
  print("Fuel: " .. gener.count())
  print("Fuel in slot: " .. robot.count(1))
  if enderchest then print("Chests: [ENDERCHEST]") else print("Chests: " .. robot.count(2)) end
  if comp.isAvailable("chunkloader") then
    if comp.chunkloader.isActive() then print("Chunkloader: ACTIVE") else print("Chunkloader: UNACTIVE") end
  else
    print("No chunkloader installed")
  end
  if comp.isAvailable("modem") and broadcastErrors then if comp.modem.isWireless() then print("Wireless modem available. Port: 23") end end
  if internet and internetErrors then print("Telegram messages enabled") end
end

function digAPI.cobblegen(owner, enderchest)
  digAPI.send("WHY I MUST WORK AS COBBLEGEN?!")
  for cobbleDigged=0,math.huge do
    if robot.count(robot.inventorySize()) == 0 then robot.slice() else os.sleep(60) end
    digAPI.info(owner, enderchest)
    print("Cobblestone mined: "..cobbleDigged)
    digAPI.recharge()
  end
end

return digAPI