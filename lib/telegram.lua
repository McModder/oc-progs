-- message api
--config
local addr = "http://api.mcmodder.ru:8080"
---------------
local inet = require("component").internet

msg = {}

function encode(code)
  checkArg(1, code, "string")
  if code then
    code = string.gsub(code, "([^%w ])", function (c)
      return string.format("%%%02X", string.byte(c))
    end)
    code = string.gsub(code, " ", "%%20")
  end
  return code 
end

function msg.send( dialogID, text )
  checkArg(1, dialogID, "string", "table")
  checkArg(2, text, "string")
  text = encode(text)
  if type(dialogID)=="string" then
    inet.request(string.format("%s/%s,%s", addr, dialogID, text))
  else
    for _,id in pairs(dialogID) do
      inet.request(string.format("%s/%s,%s", addr, id, text))
    end
  end
end

msg.addr = addr

return msg