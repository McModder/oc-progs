-- TODO:
-- версии!

local component = require("component")
local fs = require("filesystem")
local seri = require("serialization")
local shell = require("shell")

if not component.isAvailable("internet") then
  error("This program requires an internet card to run.")
  return
end

local args, ops = shell.parse(...)

-- detect ECS's MineOS
local basePath = fs.exists("/lib/MineOSCore.lua") and require("MineOSCore").paths.desktop or "/bin/"
local isMineOS = fs.exists("/lib/MineOSCore.lua") and true or nil

local function makePath( package )
  return package.dynamic and basePath..fs.name(package.link) or "/"..package.link
end

local function printHelp()
  print("Usage:")
  print("pacman --install (or -I) to install package")
  print("add --repo=RepoName to use non-default repository")
  print("add --ignore (or -i) to ignore dependencies")
  print("pacman --remove (or -R) to remove package")
  print("pacman --search (or -S) to search in repo(s)")
  print("pacman --list (or -L) to see all packages in repo(s)")
  print("pacman --update (or -U) to update installed packages")
  print("add -l to only view upgrageable packages")
  print("pacman --add to add new repo")
end

-- check args
if ops then
  local found = 0
  local toCheck = {"install", "I", "remove", "R", "search", "S", "list", "L", "add", "update", "U"}
  for _, name in pairs(toCheck) do
    if ops[name] then found = found + 1 end
  end

  if found ~= 1 then
    -- help
    printHelp()
    return
  end
else
-- help
  printHelp()
  return
end

if ops.h or ops.help then
  printHelp()
  return
end

local function internetRequest(url)
  local success, response = pcall(component.internet.request, url)
  if success then
    local responseData = ""
    while true do
      local data, responseChunk = response.read() 
      if data then
        responseData = responseData .. data
      else
        if responseChunk then
          return false, responseChunk
        else
          return true, responseData
        end
      end
    end
  else
    return false, reason
  end
end

local function makeUrl( repoData, fileLink )
  if repoData.type == "gitlab" then
    return "https://gitlab.com/"..repoData.user.."/"..repoData.repo.."/raw/"..repoData.branch.."/"..fileLink
  elseif repoData.type == "github" then
    return "https://raw.githubusercontent.com/"..repoData.user.."/"..repoData.repo.."/"..repoData.branch.."/"..fileLink
  end
end

local function download(url, filename)
  local success, reason = internetRequest(url)
  if success then
    fs.makeDirectory(fs.path(filename) or "")
    fs.remove(filename)
    local file = io.open(filename, "w")
    file:write(reason)
    file:close()
  else
    print("Can't download \"" .. filename .. "\"!\n")
    print("Reason: "..reason)
    return reason
  end
end

if not fs.exists("/etc/pacman/repolist") then
  if not fs.isDirectory("/etc/pacman") then fs.makeDirectory("/etc/pacman") end
  local repoFile = io.open("/etc/pacman/repolist", "w")
  if repoFile then
    repoFile:write("https://gitlab.com/McModder/oc-progs/raw/master/repo.txt\n")
    repoFile:close()
  else
    error("Can't write /etc/pacman/repolist!")
  end
end

function table.search( anytable, querry )
  for _, value in pairs(anytable) do
    if value == querry then return true end
  end
  return false
end

function table.count(anytable)
  local count=0
  for _ in pairs(anytable) do
    count=count+1
  end
  return count
end

-- get all repos
repos = {}
for url in io.lines("/etc/pacman/repolist") do
  local success, reason = internetRequest(url)
  if success then
    reason = seri.unserialize(reason)
    if type(reason)~="table" then print("Error! Can't fetch URL\n"..url) end
    local reponame
    if repos[reason.name] then
      local clean = false
      local iteration = 0
      repeat
        iteration = iteration + 1
        if not repos[reason.name..iteration] then clean = true end
      until clean == true
      reponame = reason.name..iteration
    else
      reponame = reason.name
    end
    repos[reponame] = {apps = reason.apps, data=reason.data}
  else
    print("Can't load apps list: "..url..", reason: "..reason)
  end
end
if table.count(repos) == 0 then
  print("ERROR: No repos found!")
  return
end
-- get installed packages list
installed = {}
if fs.exists("/etc/pacman/installedapps") then
  local file = io.open("/etc/pacman/installedapps", "r")
  installed = seri.unserialize(file:read("*a"))
  file:close()
end
installed = type(installed)=="table" and installed or {}

local function downloadPackage( repo, app )
  if repos[repo].apps[app] then
    if not download(makeUrl(repos[repo].data, repos[repo].apps[app].link), makePath(repos[repo].apps[app])) then
      print("Downloaded package "..app.." from repository "..repo)
      installed[app] = {repo=repo, version=repos[repo].apps[app].version, path=makePath(repos[repo].apps[app])}
    end
    if repos[repo].apps[app].deps and not (ops.ignore or ops.i) then
      for _, dep in pairs(repos[repo].apps[app].deps) do
        if installed[dep.name] and installed[dep.name].version==repos[dep.repo or repo].apps[dep.name or dep[1]].version then
          print("Package \""..(dep.name or dep[1]).."\" already exists")
        else
          downloadPackage(dep.repo or repo, dep.name or dep[1])
        end
      end
    end
  else
    print("No package \""..app.."\" found in repository "..repo)
  end
end

local function searchForPackage( packageName )
  local lowerPackageName = string.lower(packageName)
  for repoName, repo in pairs(repos) do
    for appName, app in pairs(repo.apps) do
      if string.lower(appName):find(lowerPackageName) then
        print("Found package "..appName.." in repository "..repoName)
        io.write("Is this the package that you need? [Y/n]")
        if ((io.read() or "n").."y"):match("^%s*[Yy]") then
          io.write("Install package now? [Y/n]")
          if ((io.read() or "n").."y"):match("^%s*[Yy]") then
            return true, appName, repoName
          else
            return false, appName, repoName
          end
        end
      end
    end
  end
  return nil, appName
end

if ops.install or ops.I then
  for _, packageName in pairs(args) do
    if repos[ops.repo or "Default"] then
      if repos[ops.repo or "Default"].apps[packageName] then
        downloadPackage(ops.repo or "Default", packageName)
      else
        print("No package \""..packageName.."\" found in repository "..(ops.repo or "Default"))
        local success, FpackageName, repoName = searchForPackage(packageName)
        if success==true then
          downloadPackage(repoName, FpackageName)
        elseif success==false then
          print("Package "..FpackageName.." installation from repository "..repoName.." canceled")
        else
          print("No package \""..packageName.."\" found")
        end
      end
    else
      print("No repository "..repo.." not found")
      local success, packageName, repoName = searchForPackage(packageName)
      if success==true then
        downloadPackage(repoName, packageName)
      elseif success==false then
        print("Package "..packageName.." installation from repository "..repoName.." canceled")
      else
        print("No package \""..packageName.."\" found")
      end
    end
  end

  local file = io.open("/etc/pacman/installedapps", "w")
  file:write(seri.serialize(installed))
  file:close()
elseif ops.remove or ops.R then
  for _, packageName in pairs(args) do
    if installed[packageName] then
      fs.remove(installed[packageName].path)
      installed[packageName] = nil
      print("Package "..packageName.." removed")
    else
      print("Package "..packageName.." not installed")
    end
  end

  local file = io.open("/etc/pacman/installedapps", "w")
  file:write(seri.serialize(installed))
  file:close()
elseif ops.search or ops.S then
  for _, packageName in pairs(args) do
    local success, packageName, repoName = searchForPackage(packageName)
    if success==true then
      downloadPackage(repoName, packageName)
    else
      print("Nothing else found")
    end
  end

  local file = io.open("/etc/pacman/installedapps", "w")
  file:write(seri.serialize(installed))
  file:close()
elseif ops.list or ops.L then
  for repoName, repo in pairs(repos) do
    print("Repository "..repoName..":")
    for packageName, package in pairs(repo.apps) do
      local status
      if installed[packageName] then
        if installed[packageName].version==package.version then
          status="[I]"
        else
          status="[U]"
        end
      end
      print("  "..packageName.." "..(package.deps and "[D]" or "")..(status or ""))
    end
  end
elseif ops.add then
  for newRepo in pairs(args) do
    if newRepo:find("(http%:%/%/.+)") or newRepo:find("(https%:%/%/.+)") then
      local newRepoFound
      local repolist = io.open("/etc/pacman/repolist", "r")
      for line in repolist:lines() do
        if line == newRepo then newRepoFound = true end
      end
      repolist:close()
      repolist = io.open("/etc/pacman/repolist", "a")
      if newRepoFound then
        print("Repo already in list, skipping")
      else
        print("New repo added")
        repolist:write(newRepo.."\n")
      end
      repolist:close()
    else
      print("It's not a repository!")
    end
  end
elseif ops.update or ops.U then
  for packageName, package in pairs(installed) do
    if not repos[package.repo] then
      print("Error updating package "..packageName..": Repo not found")
    else
      if not repos[package.repo].apps[packageName] then
        print("Error updating package "..packageName..": No such package")
      else
        if repos[package.repo].apps[packageName].version==package.version then
          print("Package "..packageName..", no update needed")
        else
          if not ops.l then
            downloadPackage(package.repo, packageName)
          else
            print("Package "..packageName..", update found")
          end
        end
      end
    end
  end

  local file = io.open("/etc/pacman/installedapps", "w")
  file:write(seri.serialize(installed))
  file:close()
end